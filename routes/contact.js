'use strict';

var Firebase = require('firebase');
var jwt = require('jwt-simple');

var contacts = new Firebase('https://address-book-restful.firebaseio.com/');

var contact = {
    add: function(req, res) {
        contacts.child('account_' + req.user.id).push({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            phone: req.body.phone
        }, function() {
            res.status(201).send('Created');
        });
    }
}

module.exports = contact;