'use strict';

var express = require('express');
var jwt = require('jwt-simple');
var router = express.Router();

var BearerStrategy = require('passport-http-bearer').Strategy;
var passport = require('passport');
var util = require('util');

var secret = 'Su7btGR9xVPHM2Jzj9b24Fegmrm59Ls';

passport.use(new BearerStrategy(
    function(token, done) {
        var decoded = jwt.decode(token, secret);
        return done(null, decoded, {
            scope: 'all'
        });
    }
));

var account = require('./account');
var contact = require('./contact');

router.post('/accounts', account.create);
router.get('/access_token', account.generate_token);

router.post('/contacts',  passport.authenticate('bearer', { session: false }), contact.add);

module.exports = router;