'use strict';

var sqlite3 = require('sqlite3').verbose();
var jwt = require('jwt-simple');
var bcrypt = require('bcrypt');

var db = new sqlite3.Database('./accounts.db');
var secret = 'Su7btGR9xVPHM2Jzj9b24Fegmrm59Ls';

var account = {
    create: function(req, res) {
        db.serialize(function() {
            var stmt = db.prepare("INSERT INTO account VALUES (?, ?, ?)"); // @To-do bcrypt
            stmt.run(req.body.email, req.body.password, function(err) {
                if (err) {
                    res.status(400).json({
                        'type': 'EmailExists',
                        'message': 'Specified e-mail address is already registered.'
                    });
                } else {
                    res.status(201).send('Created');
                }
            });
        });
    },
    generate_token: function(req, res) {
        db.all("SELECT * FROM account where email='" + req.query.email + "' and password='" + req.query.password + "'", function(err, row) {
            if (!row[0]) { // @To-do bcrypt
                res.status(401).json({
                    'type': 'InvlidEmailPassword',
                    'message': 'Specified e-mail / password combination is not valid.'
                });
            } else {
                res.status(200).send({
                    'access_token': jwt.encode({
                        'id': row[0].id
                    }, secret)
                });
            }
        });
    }
}

module.exports = account;