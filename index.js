'use strict';

// @To-do use hapi and gulp

var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());

app.use('/', require('./routes'));

app.use(function(req, res) {
	res.status(404).send('Not Found');
});

module.exports = app;